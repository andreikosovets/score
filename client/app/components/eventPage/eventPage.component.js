import template from './eventPage.html';
import controller from './eventPage.controller';
import './eventPage.less';

let eventPageComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default eventPageComponent;
