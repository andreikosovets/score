import angular from 'angular';
import uiRouter from 'angular-ui-router';
import eventPageComponent from './eventPage.component';

let eventPageModule = angular.module('eventPage', [
    uiRouter
])

    .component('eventPage', eventPageComponent)

    .name;

export default eventPageModule;
