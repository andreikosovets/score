import angular from 'angular';
import Home from './home/home';
import Login from './login/login';
import Register from './register/register';
import UserProfile from './userProfile/userProfile';
import UserResetPassword from './userResetPassword/userResetPassword'
import RegisterOrganiser from './registerOrganiser/registerOrganiser'
import OrganiserProfile from './organiserProfile/organiserProfile'
import EventPage from './eventPage/eventPage'
import AboutPage from './aboutPage/aboutPage'
import VerifyEmail from './verifyEmail/verifyEmail'
import CreateEvent from './createEvent/createEvent'
//import LoginOrganiser from './loginOrganiser/loginOrganiser'


let componentModule = angular.module('app.components', [
    Home,
    Login,
    Register,
    UserProfile,
    UserResetPassword,
    RegisterOrganiser,
    OrganiserProfile,
    EventPage,
    AboutPage,
    // LoginOrganiser,
    VerifyEmail,
    CreateEvent

])

    .name;

export default componentModule;
