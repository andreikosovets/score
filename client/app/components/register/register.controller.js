class RegisterController {
    constructor($scope, $rootScope, $state) {
        this.onloadCallback();
        this.name = 'register';
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.firebase = $rootScope.firebase;
        this.$scope.userRegister = this.userRegister;
        this.$state = $state;
    }

    onloadCallback() {
        grecaptcha.render('captcha', {
            'sitekey': '6LfPBgwUAAAAABeWKmn7To3a-dlpUdnKAzUEOYHn'
        });
    }

    userRegister(email, pass, passCinfirm) {
        let _this = this;
        let $state = this.$state;
        _this.$rootScope.isLoading = true;

        if (email === null || pass === null || passCinfirm === null) {
            _this.$rootScope.showSimpleToast("Please input data to all fields");
            _this.$rootScope.isLoading = false;
        }

        if (!grecaptcha.getResponse()) {
            _this.$rootScope.showSimpleToast("You need to accept captcha");
            _this.$rootScope.isLoading = false;
            return
        }
        if (pass !== passCinfirm) {
            _this.$rootScope.showSimpleToast("Please check your password");
            _this.$rootScope.isLoading = false;
        }
        if (email !== null && pass === passCinfirm) {
            this.firebase.auth().createUserWithEmailAndPassword(email, pass)
                .then(function successCallback(user) {
                    let curUser = firebase.auth().currentUser;
                    // add user role
                    let roleTo = +$state.params.role;
                    switch (roleTo) {
                        case 0:
                            firebase.database().ref('userRoles/' + curUser.uid)
                                .set({role: 'user'});
                            break;
                        case 1:
                            firebase.database().ref('userRoles/' + curUser.uid)
                                .set({role: 'organiser'});
                            break;
                    }
                    curUser.sendEmailVerification();
                    //add verificated email checker
                    $state.go('auth.login');
                    _this.$rootScope.isLoading = false;
                    _this.$rootScope.showSimpleToast('Successfully Registered.  Please check your email to activate your account');

                }, function errorCallback(res) {
                    _this.$rootScope.showSimpleToast(res.message);
                    _this.$rootScope.isLoading = false;
                });
        }
    };

}

RegisterController.$inject = ['$scope', '$rootScope', '$state'];

export default RegisterController;
