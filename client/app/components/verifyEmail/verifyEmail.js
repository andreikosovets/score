import angular from 'angular';
import uiRouter from 'angular-ui-router';
import verifyEmailComponent from './verifyEmail.component';

let verifyEmailModule = angular.module('verifyEmail', [
    uiRouter
])

    .component('verifyEmail', verifyEmailComponent)

    .name;

export default verifyEmailModule;
