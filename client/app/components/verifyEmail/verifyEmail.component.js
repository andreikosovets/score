import template from './verifyEmail.html';
import controller from './verifyEmail.controller';
import './verifyEmail.less';

let verifyEmailComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default verifyEmailComponent;
