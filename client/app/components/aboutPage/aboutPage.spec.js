import AboutPageModule from './aboutPage'
import AboutPageController from './aboutPage.controller';
import AboutPageComponent from './aboutPage.component';
import AboutPageTemplate from './aboutPage.html';

describe('AboutPage', () => {
    let $rootScope, makeController;

    beforeEach(window.module(AboutPageModule));
    beforeEach(inject((_$rootScope_) => {
        $rootScope = _$rootScope_;
        makeController = () => {
            return new AboutPageController();
        };
    }));

    describe('Module', () => {
        // top-level specs: i.e., routes, injection, naming
    });

    describe('Controller', () => {
        // controller specs
        it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
            let controller = makeController();
            expect(controller).to.have.property('name');
        });
    });

    describe('Template', () => {
        // template specs
        // tip: use regex to ensure correct bindings are used e.g., {{  }}
        it('has name in template [REMOVE]', () => {
            expect(AboutPageTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
        });
    });

    describe('Component', () => {
        // component/directive specs
        let component = AboutPageComponent;

        it('includes the intended template', () => {
            expect(component.template).to.equal(AboutPageTemplate);
        });

        it('invokes the right controller', () => {
            expect(component.controller).to.equal(AboutPageController);
        });
    });
});
