import angular from 'angular';
import uiRouter from 'angular-ui-router';
import registerOrganiserComponent from './registerOrganiser.component';

let registerOrganiserModule = angular.module('registerOrganiser', [
    uiRouter
])

    .component('registerOrganiser', registerOrganiserComponent)

    .name;

export default registerOrganiserModule;
