import UserResetPasswordModule from './userResetPassword'
import UserResetPasswordController from './userResetPassword.controller';
import UserResetPasswordComponent from './userResetPassword.component';
import UserResetPasswordTemplate from './userResetPassword.html';

describe('UserResetPassword', () => {
    let $rootScope, makeController;

    beforeEach(window.module(UserResetPasswordModule));
    beforeEach(inject((_$rootScope_) => {
        $rootScope = _$rootScope_;
        makeController = () => {
            return new UserResetPasswordController();
        };
    }));

    describe('Module', () => {
        // top-level specs: i.e., routes, injection, naming
    });

    describe('Controller', () => {
        // controller specs
        it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
            let controller = makeController();
            expect(controller).to.have.property('name');
        });
    });

    describe('Template', () => {
        // template specs
        // tip: use regex to ensure correct bindings are used e.g., {{  }}
        it('has name in template [REMOVE]', () => {
            expect(UserResetPasswordTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
        });
    });

    describe('Component', () => {
        // component/directive specs
        let component = UserResetPasswordComponent;

        it('includes the intended template', () => {
            expect(component.template).to.equal(UserResetPasswordTemplate);
        });

        it('invokes the right controller', () => {
            expect(component.controller).to.equal(UserResetPasswordController);
        });
    });
});
