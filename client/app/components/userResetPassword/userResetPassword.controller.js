class UserResetPasswordController {
    constructor($scope, $rootScope, $firebaseAuth) {
        this.name = 'userResetPassword';
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$firebaseAuth = $firebaseAuth;
        this.firebase = $rootScope.firebase;
        this.$scope.sendResetPasswordEmail = this.sendResetPasswordEmail.bind(this);
    }

    sendResetPasswordEmail(sendTo) {
        let _this = this,
            authObj = _this.$firebaseAuth();

        _this.$rootScope.isLoading = true;
        if (!sendTo) {
            _this.$rootScope.isLoading = false;
            _this.$rootScope.showSimpleToast("Please enter a valid email");
            return
        }
        authObj.$sendPasswordResetEmail(sendTo)
            .then(function () {
                _this.$rootScope.isLoading = false;
                _this.$rootScope.showSimpleToast("Password reset email sent successfully!");
            }, (err)=> {
                _this.$rootScope.isLoading = false;
                _this.$rootScope.showSimpleToast("Error: " + err.message);
            })
    };

}

UserResetPasswordController.$inject = ['$scope', '$rootScope', '$firebaseAuth'];
export default UserResetPasswordController;
