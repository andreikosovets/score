import template from './userProfile.html';
import controller from './userProfile.controller';
import './userProfile.less';

let userProfileComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default userProfileComponent;
