import CreateEventModule from './createEvent'
import CreateEventController from './createEvent.controller';
import CreateEventComponent from './createEvent.component';
import CreateEventTemplate from './createEvent.html';

describe('CreateEvent', () => {
    let $rootScope, makeController;

    beforeEach(window.module(CreateEventModule));
    beforeEach(inject((_$rootScope_) => {
        $rootScope = _$rootScope_;
        makeController = () => {
            return new CreateEventController();
        };
    }));

    describe('Module', () => {
        // top-level specs: i.e., routes, injection, naming
    });

    describe('Controller', () => {
        // controller specs
        it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
            let controller = makeController();
            expect(controller).to.have.property('name');
        });
    });

    describe('Template', () => {
        // template specs
        // tip: use regex to ensure correct bindings are used e.g., {{  }}
        it('has name in template [REMOVE]', () => {
            expect(CreateEventTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
        });
    });

    describe('Component', () => {
        // component/directive specs
        let component = CreateEventComponent;

        it('includes the intended template', () => {
            expect(component.template).to.equal(CreateEventTemplate);
        });

        it('invokes the right controller', () => {
            expect(component.controller).to.equal(CreateEventController);
        });
    });
});
