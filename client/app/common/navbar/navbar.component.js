import template from './navbar.html';
import controller from './navbar.controller';
import './navbar.less';
import 'angular-material/angular-material'

let navbarComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default navbarComponent;
